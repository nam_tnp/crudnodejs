const express = require('express');
const routes = require('./routes');
const app = express();

app.use(express.json());
app.use(routes);
// Handling Errors

app.listen(3000,() => console.log('Server is running on port 3000'));