const mysql = require('mysql2');
var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "password",
    database: "nodedb",
});

con.connect(function (err) {
    if (err) throw err;
    else console.log("Connect to mysql: Done!");
});
module.exports = con