Link tham khảo packet mysql2

https://www.npmjs.com/package/mysql2

create local DB: nodedb

Run this query in Mysql Workbench.

CREATE TABLE users (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  email varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  password varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY email (email)
 ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

How to use api:

POST  http://localhost:3000/register

body : 
{
  "name":"username",
  "email": "ex@gmail.com",
  "password":"password"
}

POST   http://localhost:3000/login

body : 
{
  "email": "ex@gmail.com",
  "password":"password"
}

GET   http://localhost:3000/getuser

Authorization: bearer token

Token: get response from /login api
