const router = require('express').Router();
const {body} = require('express-validator');
const {register} = require('./controllers/registerController');
const {login} = require('./controllers/loginController');
const {getUser} = require('./controllers/getUserController');

router.post('/register', [
    body('name',"The name cannot be null").notEmpty().escape().trim(),
    body('email',"Invalid email address").notEmpty().escape().trim().isEmail(),
    body('password',"The Password must be of minimum 6 characters length").notEmpty().trim().isLength({ min: 6 }),
], register);


router.post('/login',[
    body('email',"Invalid email address").notEmpty().escape().trim().isEmail(),
    body('password',"The Password must be of minimum 6 characters length").notEmpty().trim().isLength({ min: 6 }),
],login);

router.get('/getuser',getUser);

module.exports = router;